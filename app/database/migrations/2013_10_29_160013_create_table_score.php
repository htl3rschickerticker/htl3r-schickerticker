<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableScore extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('score', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('runde');
			$table->integer('heim_id');
			$table->integer('gast_id');
			$table->integer('score_heim');
			$table->integer('score_gast');
			
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('score');
	}

}
