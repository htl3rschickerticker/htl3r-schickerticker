<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSpieler extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('spieler', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name',255);
			$table->integer('tore');
			$table->string('land',255);
			$table->integer('team_id');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('spieler');
	}

}
