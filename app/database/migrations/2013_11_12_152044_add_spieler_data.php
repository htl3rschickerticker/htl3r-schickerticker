<?php

use Illuminate\Database\Migrations\Migration;

class AddSpielerData extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('spieler')->insert(array(
			'name'=> 'Alaba',
			'land'=>'Österreich',
			'tore'=>2));
		DB::table('spieler')->insert(array(
			'name'=> 'Ribery',
			'land'=>'Frankreich',
			'tore'=>4));
		DB::table('spieler')->insert(array(
			'name'=> 'Robben',
			'land'=>'Niederlande',
			'tore'=>4));
		DB::table('spieler')->insert(array(
			'name'=> 'Müller',
			'land'=>'Frankreich',
			'tore'=>2));
		DB::table('spieler')->insert(array(
			'name'=> 'Götze',
			'land'=>'Deutschland',
			'tore'=>2));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}