<?php

use Illuminate\Database\Migrations\Migration;

class AddUsersData extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('users')->insert(array(
			'name'=> 'admin',
			'password'=>'test'));

		DB::table('users')->insert(array(
			'name'=> 'tester',
			'password'=>'test'));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}