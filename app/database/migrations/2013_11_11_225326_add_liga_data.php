<?php

use Illuminate\Database\Migrations\Migration;

class AddLigaData extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('liga')->insert(array(
			'name'=> 'Bundesliga',
			'land'=>'Deutschland',
			'anzM'=>18));
		DB::table('liga')->insert(array(
			'name'=> 'English Premier League',
			'land'=>'England',
			'anzM'=>20));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}