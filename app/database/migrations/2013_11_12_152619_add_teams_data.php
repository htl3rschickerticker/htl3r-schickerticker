<?php

use Illuminate\Database\Migrations\Migration;

class AddTeamsData extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('teams')->insert(array(
			'name'=> 'FC Bayern München',
			'pos'=>1,
			'liga_id'=>1));

		DB::table('teams')->insert(array(
			'name'=> 'Bayer Leverkusen 04',
			'pos'=>2,
			'liga_id'=>2));
		DB::table('teams')->insert(array(
			'name'=> 'Borussia Dortmund',
			'pos'=>3,
			'liga_id'=>3));
			DB::table('teams')->insert(array(
			'name'=> 'Bor. Mönchengladbach',
			'pos'=>4,
			'liga_id'=>4));
		DB::table('teams')->insert(array(
			'name'=> 'Vfl Wolfsburg',
			'pos'=>5,
			'liga_id'=>5));
		DB::table('teams')->insert(array(
			'name'=> 'FC Schalke',
			'pos'=>6,
			'liga_id'=>6));
		DB::table('teams')->insert(array(
			'name'=> 'FSV Mainz 05',
			'pos'=>7,
			'liga_id'=>7));
		DB::table('teams')->insert(array(
			'name'=> 'Hertha BSC Berlin',
			'pos'=>8,
			'liga_id'=>8));
		DB::table('teams')->insert(array(
			'name'=> 'Vfb Stuttgart',
			'pos'=>9,
			'liga_id'=>9));
		DB::table('teams')->insert(array(
			'name'=> 'FC Augsburg',
			'pos'=>10,
			'liga_id'=>10));
		DB::table('teams')->insert(array(
			'name'=> 'Hamburger SV',
			'pos'=>11,
			'liga_id'=>11));
		DB::table('teams')->insert(array(
			'name'=> 'SV Werder Bremen',
			'pos'=>12,
			'liga_id'=>12));
		DB::table('teams')->insert(array(
			'name'=> 'Hannover 96',
			'pos'=>13,
			'liga_id'=>13));
		DB::table('teams')->insert(array(
			'name'=> 'TSG 1899 Hoffenheim',
			'pos'=>14,
			'liga_id'=>14));
		DB::table('teams')->insert(array(
			'name'=> 'Eintracht Frankfurt',
			'pos'=>15,
			'liga_id'=>15));
		DB::table('teams')->insert(array(
			'name'=> 'Eintracht Frankfurt',
			'pos'=>15,
			'liga_id'=>15));



	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}