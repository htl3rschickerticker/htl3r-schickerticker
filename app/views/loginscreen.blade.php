@extends("master")
@section("content")
{{Form::open(array('action' => 'UserController@loginAction'))}}

{{Form::label('username', 'Benutzername')}}

{{Form::text('username', 'Max Mustermann')}}



@if($errors->has('benname'))
<span>{{ $errors->first('benname') }}</span>
@endif

<br />
{{Form::label('password', 'Password')}}

{{Form::password('password')}}

@if($errors->has('pword'))
<span>{{ $errors->first('pword') }}</span>
@endif

<br />

{{Form::submit('Login')}}
{{link_to_action('UserController@registrierAction', 'Registrieren', $attributes = array(), $secure = null)}}


{{Form::close()}}
@stop