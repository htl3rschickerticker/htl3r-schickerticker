<?php

class Liga extends Eloquent 
{
	protected $table = 'liga';

	public function teams()
	{
		return $this->hasMany('Team');
	}

}