<?php

class Team extends Eloquent 
{
	protected $table = 'teams';

	public function liga()
	{
		return $this->belongsTo('Liga');
	}

	public function spieler()
	{
		return $this->hasMany('Spieler');
	}	

}