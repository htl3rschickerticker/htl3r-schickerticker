<?php

class Spieler extends Eloquent 
{
	protected $table = 'spieler';

	public function spieler()
	{
		return $this->belongsTo('Team');
	}

}