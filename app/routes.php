<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::group(array('before' => 'auth'), function()
{
	Route::get('/', 'HomeController@showLigen');
	Route::get('/ligen', 'HomeController@showLigen');
	Route::get('/liga/{id}', 'HomeController@showLiga');
	Route::get('/mannschaft/{id}', 'HomeController@showMannschaft');
});



Route::get('/login', function()
{
    return View::make('loginscreen');
});

Route::get('/registration', function()
{
    return View::make('registrieren');
});

Route::post('/login', 'UserController@loginAction');
Route::post('/logout', 'UserController@logoutAction');
Route::post('/registration', 'UserController@registrierAction');
