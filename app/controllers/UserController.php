<?php

class UserController extends BaseController {


	 public function loginAction()
	 {
	 	$bname = Input::get('username');
	 	$pw = Input::get('password');
	 

		 		if(Auth::attempt(array('name' => $bname, 'password' => $pw), true))
		 		{
		 			return Redirect::to('/');
		 		}
	 			else{
	 			return Redirect::to('/login')->withErrors();
	 			}
	 		
	 }

	 public function logoutAction()
	 {
	 	Auth::logout();
	 	return Redirect::to('/login');
	 }

	 public function registrierAction()
	 {
	 	$bname = Input::get('username');
	 	$pw = Input::get('password');
	 		$validator = Validator::make(
    			array('name' => $bname,
    					'pw' => $pw),
    			array('name' => 'required|min:2|max:20|alpha_num|unique:users,name',
    					'pw' => 'required|min:4|max:32|alpha_num|unique:users,name')
			);
			if ($validator->passes())
			{
				DB::table('users')->insert(array(
				'name'=> $bname,
				'password'=>Hash::make($pw)));

	 			return Redirect::to('/login');
			}
			else{
				
	 			$messages = $validator->messages();
    			return Redirect::to('/registration')->withErrors($validator);
	 		}
	 	
	 }
}