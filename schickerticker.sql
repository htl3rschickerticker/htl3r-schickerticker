-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 03. Jun 2014 um 16:33
-- Server Version: 5.5.27
-- PHP-Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `schickerticker`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `liga`
--

CREATE TABLE IF NOT EXISTS `liga` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `land` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `liga`
--

INSERT INTO `liga` (`id`, `name`, `land`, `created_at`, `updated_at`) VALUES
(1, 'Bundesliga', 'Deutschland', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'English Premier League', 'England', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2013_10_29_152200_create_table_team', 1),
('2013_10_29_155611_create_table_liga', 1),
('2013_10_29_155820_create_table_spieler', 1),
('2013_10_29_160013_create_table_score', 1),
('2013_11_11_225326_add_liga_data', 1),
('2013_11_12_152044_add_spieler_data', 1),
('2013_11_12_152619_add_teams_data', 1),
('2013_11_26_095005_create_users_table', 1),
('2013_11_26_095232_add_users_data', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `score`
--

CREATE TABLE IF NOT EXISTS `score` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `runde` int(11) NOT NULL,
  `heim_id` int(11) NOT NULL,
  `gast_id` int(11) NOT NULL,
  `score_heim` int(11) NOT NULL,
  `score_gast` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `spieler`
--

CREATE TABLE IF NOT EXISTS `spieler` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tore` int(11) NOT NULL,
  `land` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Daten für Tabelle `spieler`
--

INSERT INTO `spieler` (`id`, `name`, `tore`, `land`, `team_id`) VALUES
(1, 'Alaba', 2, 'Österreich', 1),
(2, 'Ribery', 4, 'Frankreich', 1),
(3, 'Robben', 4, 'Niederlande', 1),
(4, 'Müller', 2, 'Frankreich', 1),
(5, 'Götze', 2, 'Deutschland', 1),
(6, 'Lewandowski', 19, 'Polen', 2),
(7, 'Weidenfeller', 0, 'Deutschland', 2),
(8, 'Hummels', 1, 'Deutschland', 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `teams`
--

CREATE TABLE IF NOT EXISTS `teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pos` int(11) NOT NULL,
  `liga_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Daten für Tabelle `teams`
--

INSERT INTO `teams` (`id`, `name`, `pos`, `liga_id`) VALUES
(1, 'FC Bayern München', 1, 1),
(2, 'Dortmund', 2, 1),
(3, 'Bayer 04 Leverkusen', 3, 1),
(4, 'Bor. Mönchengladbach', 4, 1),
(5, 'Vfl Wolfsburg', 5, 1),
(6, 'Schalke', 6, 1),
(7, 'Hertha BSC Berlin', 7, 1),
(8, 'Vfb Stuttgart', 8, 1),
(9, '1. FSV Mainz 05', 9, 1),
(10, 'Werder Bremen', 10, 1),
(11, 'Hannover 96', 11, 1),
(12, '1899 Hoffenheim', 12, 1),
(13, 'FC Augsburg', 13, 1),
(14, 'Hamburger SV', 14, 1),
(15, 'Arsenal FC', 1, 2),
(16, 'Aston Villa FC', 2, 2),
(17, 'Chelsea FC', 3, 2),
(18, 'Liverpool FC', 4, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=44 ;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `name`, `password`) VALUES
(1, 'admin', '$2y$08$ssUzIHdP3nmlGF4EOQsG1eaQ9g80CSsagiYqDMHYGeYuM8cTm4VcO'),
(2, 'tester', '$2y$08$0xZ6JKW8lMGryOsXbGbs9ebrsUxixzPIO3stGmXqg0begw7RyrTNW'),
(31, 'Testak', '$2y$08$GAH1Tzm9tY8THU1lQe8QQuzq1xD4ao25CmC.GThoWGpWqdKBOocYa'),
(36, 'cooler', '$2y$08$QnSw6ntJKnjiBIBZrawRweT1Trn5hCFiv.53V.6zpaqmKJvUjHyHy'),
(37, 'Lopez', '$2y$08$5me/K1vOiM7985wfHgeg3OqF50nAlcIciYDA/kF6ecjUFk7xD.Dru'),
(39, 'Chris', '$2y$08$DN/JK1MxQ6AtC4.APc7BquwNlQwDsSlxK6576eC8gx0IbBWdj4pwK'),
(40, 'Bernhard', '$2y$08$1taceA/XK.Jlkbs/tPCDkuGWf6078uoKL776V2/CzvEpUQQKr1I36'),
(41, 'michael', '$2y$08$P8wHL5.tX3NNFosTJ0NOAOLq4QKu/b/lkCVadv.v4cEOCOK/lxHR6'),
(42, 'yoloswag', '$2y$08$KlHkr2fzIQ1TeJuaT9tJ0O9pJMwEaxM.9XOAszPmkFtkBmfv4sElS'),
(43, 'josef', '$2y$08$yu2qWaa/KDO3g7Bhpqv05.IHJmGetGlqKq2HHvyWzsJrLixeYEptK');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
